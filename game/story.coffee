cloak = unit "cloak",
  examine: () -> "cloak".l()
  display: () -> "cloak_disp".l()
  inv: () -> "cloak".l()
  dsc: ""
  takeable: true
  take: () ->
    cloak = null
    for thing in salet.here().units
      if thing.name == "cloak"
        cloak = thing
    salet.here().drop('cloak')
    salet.character.take(cloak)
    return "take_cloak".l()
  drop: () ->
    if (salet.current != 'cloakroom')
      return "drop_cloak".l()
    cloak = null
    for thing in salet.character.inventory
      if thing.name == "cloak"
        cloak = thing
    salet.character.drop('cloak')
    salet.here().take(cloak)
    return "hang_cloak".l()
  invl: () -> "cloak_invl".l()
  wear: () ->
    if (salet.here().has('cloak'))
      cloak = null
      for thing in salet.here().units
        if thing.name == "cloak"
          cloak = thing
      salet.here().drop('cloak')
      salet.character.take(cloak)
      return "wear_cloak".l()
    else # no cloak in the room, maybe in the inventory?
      if salet.character.has('cloak')
        return "wear_cloak".l()
      else
        return "no_cloak".l()

foyer_options =
  before: () -> "start".l()
  title: () -> "foyer_title".l()
  dsc: () -> "foyer".l()
  ways: ["entrance", "cloakroom", "bar"]
  canExit: (to) -> # can't exit to north
    if (to == 'entrance' and salet.interactive)
      # print the effect after the room description, after the player enters the room
      setTimeout(() ->
        salet.view.write("<span class='effect'>"+"entrance".l()+"</span>")
      , 0)
      return false

croom "start", foyer_options
foyer_options.before = null # no title splash after game begins
croom "foyer", foyer_options

croom "cloakroom",
  dsc: () -> "cloakroom".l()
  title: () -> "cloakroom_title".l()
  ways: ["foyer"]
  units: []

hook = unit "hook",
  dsc: "",
  display: () -> "hook_disp".l()
  examine: () ->
    if salet.here().has('cloak')
      list = ""
      for thing in salet.here().units
        if thing.name == "cloak"
          list += thing.invl()
      return "hook".l() + "hook_full".l({
        "list": list
      })
    else
      return "hook".l() + "hook_empty".l()
hook.put("cloakroom")

croom "entrance",
  title: () -> "entrance_title".l()
  ways: ["foyer"]
  dsc: () -> "entrance_secret".l()

croom "bar",
  onEnter: () ->
    if salet.character.has('cloak')
      this.dsc = "bar_dark".l()
      this.units = [
        unit "darkness",
          display: () -> "darkness".l()
          dsc: ""
          examine: () ->
            this.chaos++
            "dark".l()
          takeable: true
          take: () ->
            this.chaos++
            "dark".l()
          wear: () ->
            "wear_darkness".l()
      ]
      this.actions = {}
    else
      this.actions = {
        message: () ->
          salet.view.removeTransient()
          salet.view.write salet.here().units[0].act()
      }
      this.dsc = "bar".l()
      this.units = [
        unit "message",
          display: () -> "message_disp".l()
          dsc: () -> "message".l()
          act: () ->
            $(".sidebar").hide()
            $("#content").toggleClass("narrow")
            $(".ways").hide()
            if salet.here().chaos == 0
              if salet.interactive and _paq?
                _paq.push(['setCustomDimension', 1, true]);
              return "message_x".l()
            else
              if salet.interactive and _paq?
                _paq.push(['setCustomDimension', 1, false]);
              return "message_ruined".l()
          examine: () ->
            salet.view.removeTransient()
            salet.here().units[0].act()
      ]
    salet.character.update_sidebar()
    return ""
  title: () -> "bar_title".l()
  ways: ["foyer"]
  chaos: 0
