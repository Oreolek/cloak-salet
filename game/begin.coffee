salet.game_id = "8b0c371c-57f4-49b3-ae3c-cba07d1a9733"
salet.game_version = "1.0"
salet.verbRe = /^verb\_(\w+)\_(\w+)$/
salet.beforeAction = (room, actionId) ->
  match = salet.verbRe.exec(actionId)
  if match? and match[1] and match[2]
    verb = match[1]
    unit = match[2]
    if room.has(unit)
      salet.view.write(room.unitdo(unit, verb))
    else if salet.character.has(unit)
      for i in salet.character.inventory
        if i.name == unit
          salet.view.write(i[verb].fcall(salet.rooms[room.name]))
    return true # consume the action
  return false
salet.afterAction = () ->
  salet.character.update_sidebar()

$.holdReady( true )
$.getJSON('game/translations/'+i18n.lang+'.json', (data) ->
  i18n.push(i18n.lang, data)

  $.holdReady( false )
)

switchTab = (tabid) ->
  $(".tab").removeClass("active")
  $("#"+tabid).addClass("active")
  if tabid == "storytab" and not salet.here().canSave
    salet.goBack()

$(document).ready(() ->
  window.addEventListener('popstate', (event) ->
    salet.goBack()
  )
  $("body").on("click", '#night', () ->
    if (window.night)
      $("body").removeClass("night")
      $("#night").removeClass("active")
      window.night = false
    else
      $("body").addClass("night")
      $("#night").addClass("active")
      window.night = true
  )
  $("body").on("click", ".tab", (event) ->
    switchTab(event.target.id)
    return true
  )
  $("#page").on("click", "a", (event) ->
    if (window.hasOwnProperty('TogetherJS') and !window.remote and TogetherJS.running)
      options = {
        type: "click"
      }
      link = $(event.target)
      if link.attr("id") != undefined
        options.id = link.attr("id")
      if link.attr("href") != undefined
        options.href = link.attr("href")
      if options.href == undefined and options.id == undefined
        return
      TogetherJS.send(options)
  )
  window.remote = false
  if (window.hasOwnProperty('TogetherJS'))
    TogetherJS.config("ignoreForms", true)
    TogetherJS.config("ignoreMessages", [
      "cursor-update"
      "keydown"
      "scroll-update"
      "form-focus"
      "cursor-click"
    ])
    TogetherJS.hub.on("click", (msg) ->
      if (! msg.sameUrl)
        return
      window.remote = true
      if msg.id != undefined
        $("##{msg.id}").trigger("click")
      else if msg.href != undefined
        $("#page a[href=#{msg.href}]").trigger("click")
      window.remote = false
    )
  salet.beginGame()
)

salet.init = () ->
  @character.displayAll = false
  @character.take(cloak)
  @character.update_sidebar = () ->
    if @has('cloak')
      $("#tentacles").show()
      $("body").addClass("darker")
    else
      $("body").removeClass("darker")
      $("#tentacles").hide()
    $(".objects").empty()
    for obj in document.querySelectorAll(".objects")
      for u in salet.here().units
        if u != null
          updateverb(u, $(obj).data("verb"))
      for u in salet.character.inventory
        if u != null
          updateverb(u, $(obj).data("verb"))
    pruneVerbs()

updateverb = (unit, verb) ->
  if unit[verb]? or salet.character.displayAll
    switch verb
      when "take"
        if unit.takeable == false or salet.character.has(unit.name)
          return ""
      when "drop"
        if not salet.character.has(unit.name)
          return ""
      when "wear"
        if salet.character.has(unit.name)
          return ""
    $("##{verb}list").append("<li><a href='./verb_#{verb}_#{unit.name}'>#{unit.display()}</a></li>")

pruneVerbs = () ->
  for verb in document.querySelectorAll(".verb")
    text = $(verb).data("verb")
    if $("##{text}list").html() == ""
      verb.style.cssText = "visibility: hidden"
    else
      verb.style.cssText = "visibility: visible"

###
Element helpers. There is no real need to build monsters like a().id("hello")
because you won't use them as is. It does not make sense in context, the
author has Markdown and all utilities to *forget* about the markup.
###
way_to = (content, ref) ->
  return "<a href='#{ref}' class='way'>#{content}</a>"
textlink = (content, ref) ->
  return "<a href='./_writer_#{ref}' class='once'>#{content}</a>"
actlink = (content, ref) ->
  return "<a href='./#{ref}' class='once'>#{content}</a>"

sysroom = (name, options) ->
  options.canSave = false
  options.enter = () ->
    if salet.interactive and _paq?
      # Piwik analytics: room stats
      _paq.push(['trackPageView', name])
    $(".action").hide()
  options.exit = () ->
    if document.querySelector('#current-room')
      salet.view.clearContent('#current-room')
    $(".action").show()
  options.dsc = () ->
    return @text.fcall()
  ###
      +"\n\n"
      +"""
    <div class="center"><a href="./exit"><button class="btn btn-lg btn-outline-primary">Go back</button></a></div>
    """
  options.actions = {
    exit: () ->
      return salet.goBack()
  }
  ###
  return room(name, options)

croom = (name, spec) ->
  spec.clear ?= true
  spec.optionColor ?= ""
  spec.has = (thing) ->
    for item in this.units
      if item.name == thing
        return true
    return false
  spec.unitdo = (thing, verb) ->
    for item in this.units
      if item.name == thing and item[verb] != undefined
        return item[verb].fcall(this)
  spec.optionText ?= () -> 
    retval = """
      <div class="#{spec.optionColor}">
        <div class="title">#{spec.title.fcall()}</div>
    """
    if (spec.subtitle?)
      retval += """
        <div class="subtitle">#{spec.subtitle.fcall()}</div>
      """
    retval += '</div>'
  spec.enter = () ->
    if salet.interactive and _paq?
      # Piwik analytics: room stats
      _paq.push(['trackPageView', name])
    salet.character.update_sidebar()
    if @onEnter?
      @onEnter()
  return room(name, spec)

sysroom "inventory",
  text: () ->
    if salet.character.inventory.length == 0
      text = "inventory_empty".l()
    else
      text = "inventory_contains".l()+"\n\n"
      for thing in salet.character.inventory
        text += "* #{salet.character.listinv(thing.name)}\n"

sysroom "settings",
  text: () -> 
    nightclass = ""
    if window.night
      nightclass = "active"
    return "credits".l() + """\n
      <button id="night" class="btn btn-outline-primary #{nightclass}">#{"night".l()}</button>

      <button onclick="TogetherJS(this); return false;" class="btn btn-outline-primary">#{"multiplayer".l()}</button>
    """

sysroom "map",
  text: () ->
    return "<div id='map'></div>"
  after: () ->
    data = {
      edges: []
      nodes: []
    }
    edges = []
    rooms = []
    globx = 1
    globy = 1
    deltas = [
      # [1, 0], # looks bad on our map
      [0, 1],
      [-1, 0],
      [0, -1],
    ]
    for name, room of salet.rooms
      if room.canSave == false or name == "start"
        continue
      if rooms.indexOf(name) == -1
        data.nodes.push({
          "id": name
          "label": room.title()
          "size": 5
          "color": "#000"
          "x": globx
          "y": globy
        })
        rooms.push(name)
      if room.ways? and room.ways.length > 0
        delta = 0
        for way in room.ways
          id = "edge_"+name+"_"+way
          # we don't want to display a two-way link twice
          if edges.indexOf("edge_"+way+"_"+name) == -1
            edges.push(id)
            data.edges.push({
              "id": id
              "source": room.name
              "target": way
              "size": 1
              "color": "#ccc"
            })
          if rooms.indexOf(way) == -1
            data.nodes.push({
              "id": way
              "label": salet.rooms[way].title()
              "size": 5
              "color": "#000"
              "x": globx + deltas[delta][0]
              "y": globy + deltas[delta][1]
            })
            rooms.push(way)
            delta++
        globy = globy - 2
    s = new sigma({
      graph: data,
      container: 'map'
    })
    s.bind('clickNode', (e) ->
      switchTab("storytab")
      salet.goTo(e.data.node.id)
    )
    return ""
